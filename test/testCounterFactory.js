const { describe } = require('mocha');
const assert = require('chai').assert;
const counterFactory = require('../counterFactory');

describe('counterFactory', () => {
    it('For no arguments passed, 0 should be assigned to counter', () => {
        const factory = counterFactory();
        assert.equal(factory.increment(), 1);
        assert.equal(factory.increment(), 2);
        assert.equal(factory.decrement(), 1);
        assert.equal(factory.decrement(), 0);
    });

    it('For a number argument passed, argument assigned to counter', () => {
        const factory = counterFactory(21);
        assert.equal(factory.increment(), 22);
        assert.equal(factory.increment(), 23);
        assert.equal(factory.decrement(), 22);
        assert.equal(factory.decrement(), 21);
    });

    it('For number passed as string, convert to number and work', () => {
        const factory = counterFactory('27');
        assert.equal(factory.increment(), 28);
        assert.equal(factory.increment(), 29);
        assert.equal(factory.decrement(), 28);
        assert.equal(factory.decrement(), 27);
    });

    it('Function shouln\'t throw error for non number or non string arguments and assign 0 as counter', () => {
        const factory = counterFactory([2, 4, 5, 6]);
        const factory2 = counterFactory({ name: 'Mountblue' });
        assert.equal(factory.increment(), 1);
        assert.equal(factory.increment(), 2);
        assert.equal(factory.decrement(), 1);
        assert.equal(factory.decrement(), 0);
        assert.equal(factory2.decrement(), -1);
        assert.equal(factory2.decrement(), -2);
        assert.equal(factory2.decrement(), -3);
    });

    it('Function should increment and decrement floating-point number as float or string as well if passed as argument by assigning it as counter', () => {
        const factory = counterFactory(21.21);
        const factory2 = counterFactory('21.21');
        assert.equal(factory.increment(), 22.21);
        assert.equal(factory.increment(), 23.21);
        assert.equal(factory.decrement(), 22.21);
        assert.equal(factory.decrement(), 21.21);
        assert.equal(factory2.increment(), 22.21);
        assert.equal(factory2.increment(), 23.21);
        assert.equal(factory2.decrement(), 22.21);
        assert.equal(factory2.decrement(), 21.21);
    });

});
