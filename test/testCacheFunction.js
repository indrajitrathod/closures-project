let cacheFunction = require('../cacheFunction');

let cachedFunction1 = cacheFunction((arg) => {
    if (typeof arg === 'object' && !Array.isArray(arg)) {
        arg = JSON.stringify(arg);
    }
    return `arg type: ${typeof (arg)}, & arg is: ${arg}\n`;
});


console.log(cachedFunction1(1));
console.log(cachedFunction1('Mountblue'));
console.log(cachedFunction1([1, 2, 3, 4, 5]));
console.log(cachedFunction1(1, 2, 3));
console.log(cachedFunction1({ name: 'Mountblue', course: 'JavaScript', mentor: 'Fabian Enos' }));
console.log(cachedFunction1([1, 2, 3, 4, 5]));
console.log(cachedFunction1({ name: 'Mountblue', course: 'JavaScript', mentor: 'Fabian Enos' }));
