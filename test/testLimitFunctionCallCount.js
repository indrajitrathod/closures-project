const { describe } = require('mocha');
const assert = require('chai').assert;

const limitFunctionCallCount = require('../limitFunctionCallCount');

describe('limitFunctionalCallCount', () => {
    it('Function should be invoked 4 times & 5th time onwards should return null', () => {

        const callbackLimit = limitFunctionCallCount(() => {
            console.log(`I am invoked`);
        }, 4);

        assert.isNotNull(callbackLimit());
        assert.isNotNull(callbackLimit());
        assert.isNotNull(callbackLimit());
        assert.isNotNull(callbackLimit());

        assert.isNull(callbackLimit());
        assert.isNull(callbackLimit());
        assert.isNull(callbackLimit());

    });

    it('Function should return null when callback function is not passed', () => {

        const callbackLimit = limitFunctionCallCount(1, 4);
        const callbackLimit2 = limitFunctionCallCount('Hello', 4);
        const callbackLimit3 = limitFunctionCallCount(true, 4);
        const callbackLimit4 = limitFunctionCallCount([2, 4, 5, 7], 4);
        const callbackLimit5 = limitFunctionCallCount({ name: 'Mountblue' }, 4);

        assert.isNull(callbackLimit());
        assert.isNull(callbackLimit2());
        assert.isNull(callbackLimit3());
        assert.isNull(callbackLimit4());
        assert.isNull(callbackLimit5());

    });

    it('Function should be invoked 3 times when 3.3 is passed as limit, by flooring the limit value', () => {

        const callbackLimit = limitFunctionCallCount(() => {
            console.log(`I am invoked`);
        }, 3.3);

        assert.isNotNull(callbackLimit());
        assert.isNotNull(callbackLimit());
        assert.isNotNull(callbackLimit());

        assert.isNull(callbackLimit());
        assert.isNull(callbackLimit());

    });

    it('Function should be invoked when n is a number in string format', () => {

        const callbackLimit = limitFunctionCallCount(() => {
            console.log(`I am invoked`);
        }, '1.3');

        assert.isNotNull(callbackLimit());
        assert.isNull(callbackLimit());

    });

    it('Function shouldn\'t throw error and return null when n (limit) is not a valid number', () => {

        const callbackLimit = limitFunctionCallCount(() => {
            console.log(`I am invoked`);
        }, '$564');

        const callbackLimit2 = limitFunctionCallCount(() => {
            console.log(`I am invoked`);
        }, [1.34]);

        const callbackLimit3 = limitFunctionCallCount(() => {
            console.log(`I am invoked`);
        }, true);

        const callbackLimit4 = limitFunctionCallCount(() => {
            console.log(`I am invoked`);
        }, false);

        const callbackLimit5 = limitFunctionCallCount(() => {
            console.log(`I am invoked`);
        }, { name: 'Indrajit' });

        assert.isNull(callbackLimit());
        assert.isNull(callbackLimit2());
        assert.isNull(callbackLimit3());
        assert.isNull(callbackLimit4());
        assert.isNull(callbackLimit5());
    });

    it('Check with arguments passed', () => {

        const callbackLimit = limitFunctionCallCount((arg1, arg2) => {
            console.log(`I am invoked with arguments - ${arg1} & ${arg2}`);
        }, 3);

        const callbackLimit2 = limitFunctionCallCount((obj1, obj2) => {
            console.log(`I have object arguments - ${obj1.name} & ${obj2.name}`);
        }, 1);

        assert.isNotNull(callbackLimit(1, 2));
        assert.isNotNull(callbackLimit('I am string1', 'I am string2', 'This is 3rd string'));
        assert.isNotNull(callbackLimit([1, 2, 3, 4, 5], [5, 6, 7, 8, 9]));
        assert.isNull(callbackLimit([1, 2, 3], [5, 6]));

        assert.isNotNull(callbackLimit2({ name: 'Mountblue' }, { name: 'JavaScript' }));
        assert.isNull(callbackLimit2({ name: 'Mountblue2' }, { name: 'Java' }));
        assert.isNull(callbackLimit2({ name: 'Mountblue3' }, { name: 'Python' }));

        const limitedAddFunction = limitFunctionCallCount((...numbers) => {
            let sum = 0;

            for (let i = 0; i < numbers.length; i++) {
                sum += numbers[i];
            }

            return sum;
        }, 1);

        let sumAddition;

        sumAddition = limitedAddFunction(1, 2, 3, 4, 5);
        assert.equal(sumAddition, 15);

        sumAddition = limitedAddFunction(1, 2, 3);
        assert.isNull(sumAddition);

    });
});
