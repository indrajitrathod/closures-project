const counterFactory = (number) => {
    let counter = 0;
    if (number && !isNaN(Number(number))) {
        counter = Number(number);
    }

    const increment = () => {
        return ++counter;
    }
    const decrement = () => {
        return --counter;
    }

    return {
        increment,
        decrement
    }
}

module.exports = counterFactory;