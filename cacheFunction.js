function cacheFunction(cb) {

    if(typeof cb !== 'function'){
        return;
    }

    let cache = {};

    const cachedFunction = (...args) => {

        const key = JSON.stringify(...args);
        let result;

        if (cache.hasOwnProperty(key)) {
            result = cache[key];
        
        } else {
            result = cb(...args);
            cache[key] = result;
        }

        return result;
    }

    return cachedFunction;
}

module.exports = cacheFunction;

