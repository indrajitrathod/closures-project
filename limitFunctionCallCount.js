function limitFunctionCallCount(cb, n) {

    let limit;

    if (n !== undefined && (typeof n === 'number' || typeof n === 'string') && !isNaN(n)) {
        limit = Math.floor(Number(n));
    }

    const limitFunction = (...args) => {

        if (cb !== undefined && typeof cb === 'function' && !isNaN(limit)) {
            if (limit > 0) {
                limit--;
                return cb(...args);
            }
        }

        return null;
    }

    return limitFunction;
}

module.exports = limitFunctionCallCount;
